package com.example.maury.mejorandroidapis;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.maury.mejorandroidapis.db.DBHelper;
import com.example.maury.mejorandroidapis.db.DBOperations;
import com.example.maury.mejorandroidapis.models.Tweet;
import com.example.maury.mejorandroidapis.utils.ConstantsUtils;
import com.example.maury.mejorandroidapis.utils.TwitterUtils;

import java.util.ArrayList;

public class UpdaterService extends Service {
    private static final String TAG = UpdaterService.class.getSimpleName();
    static final int DELAY = 60000;
    private boolean runFlag = false;
    private Updater updater;
    private MejorandroidApplication application;
    private DBOperations dbOperations;

    // En un servicio ligado este debe retornar una referencia a la actividad
    // En servicio desligado debe retornar null
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        updater = new Updater();
        application = (MejorandroidApplication) getApplication();
        dbOperations = new DBOperations(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        runFlag = false;
        application.setServiceRunningFlag(false);
        updater.interrupt();
        updater = null;
    }

    // Este metodo se ejecuta la primer vez que corro el servicio despues del onCreate
    // y siempre que quiero correr el servicio aunque ya este creado
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d(TAG, "onStartCommand");
        runFlag = true;
        application.setServiceRunningFlag(true);
        updater.start();
        // Indica que el servicio se deve volver a ejecutar si por algo falla
        return START_REDELIVER_INTENT;
    }

    // Para lograr que este servicio no afecte al hilo principal de interface uso otro hilo
    private class Updater extends Thread{
        private ArrayList<Tweet> timeline = new ArrayList<>();
        private Intent intent;

        public Updater(){
            super("UpdaterService-UpdaterThread"); // Esta es una etiqueta para identificar el hilo
        }

        @Override
        public void run() {
            UpdaterService updaterService = UpdaterService.this;

            while(updaterService.runFlag){
                Log.d(TAG, "UpdaterThread is running");
                try{
                    timeline = TwitterUtils.getTimelineForSearchTerm(ConstantsUtils.MEJORANDROID_TERM);

                    ContentValues values = new ContentValues();

                    for(Tweet tweet : timeline){
                        values.clear();

                        values.put(DBHelper.C_ID, tweet.getId());
                        values.put(DBHelper.C_NAME, tweet.getName());
                        values.put(DBHelper.C_SCREEN_NAME, tweet.getScreenName());
                        values.put(DBHelper.C_IMAGE_PROFILE_URL, tweet.getProfileImageUrl());
                        values.put(DBHelper.C_TEXT, tweet.getText());
                        values.put(DBHelper.C_CREATED_AT, tweet.getCreatedAt());

                        Log.d(TAG, "CREATED AT SERVICE: " + tweet.getCreatedAt());

                        dbOperations.insertOrIgnore(values);
                    }

                    // Avisandole al UI que hay nuevos tweets para que se refresque
                    intent = new Intent(ConstantsUtils.NEW_TWEETS_INTENT_FILTER);
                    sendBroadcast(intent);

                    Thread.sleep(DELAY);
                }catch(InterruptedException e){
                    updaterService.runFlag = false;
                    application.setServiceRunningFlag(false);
                }
            }
        }
    }
}
