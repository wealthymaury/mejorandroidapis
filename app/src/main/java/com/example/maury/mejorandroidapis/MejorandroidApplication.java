package com.example.maury.mejorandroidapis;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

/*
 * Esta clase se activa cuando se inicia el sistema y tu app tiene algun broadcast reciver
 * Ademas se destruye cuando tu cierras tu app a la brava
 */

public class MejorandroidApplication extends Application {
    private static final String TAG = MejorandroidApplication.class.getSimpleName();
    private boolean serviceRunningFlag;

    public boolean isServiceRunning(){
        return serviceRunningFlag;
    }

    public void setServiceRunningFlag(boolean serviceRunningFlag){
        this.serviceRunningFlag = serviceRunningFlag;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        // Al arrancar cualquier componente de esta clase se habilita el servicio
        startService(new Intent(this, UpdaterService.class));
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
        // Que al cerrar mi app se detenga el servicio
        stopService(new Intent(this, UpdaterService.class));
    }
}
