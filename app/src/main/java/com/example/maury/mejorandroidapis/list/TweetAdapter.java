package com.example.maury.mejorandroidapis.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maury.mejorandroidapis.R;
import com.example.maury.mejorandroidapis.models.Tweet;
import com.example.maury.mejorandroidapis.utils.BitmapManager;
import com.example.maury.mejorandroidapis.utils.DateUtils;

import java.util.ArrayList;

public class TweetAdapter extends ArrayAdapter<Tweet> {
    private Context context;
    private ArrayList<Tweet> tweets;

    public TweetAdapter(Context context, int view_resource_id, ArrayList<Tweet> tweets){
        super(context, view_resource_id, tweets);
        this.context = context;
        this.tweets = tweets;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Inicializo la vista solo la primer vez
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.row_tweet, parent, false);

            ViewHolder view_holder = new ViewHolder();
            view_holder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
            view_holder.name = (TextView) convertView.findViewById(R.id.name);
            view_holder.screen_name = (TextView) convertView.findViewById(R.id.screen_name);
            view_holder.text = (TextView) convertView.findViewById(R.id.text);
            view_holder.created_at = (TextView) convertView.findViewById(R.id.created_at);

            convertView.setTag(view_holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        BitmapManager.getInstance().loadBitmap(tweets.get(position).getProfileImageUrl(), holder.avatar);
        holder.name.setText(tweets.get(position).getName());
        holder.screen_name.setText("@" + tweets.get(position).getScreenName());
        holder.text.setText(tweets.get(position).getText());
        holder.created_at.setText(DateUtils.setDateFormat(tweets.get(position).getCreatedAt()));

        return super.getView(position, convertView, parent);
    }

    /* Texnica utilizada para un rendimiento optimo con el scroll de la lista */
    static class ViewHolder {
        public ImageView avatar;
        public TextView name;
        public TextView screen_name;
        public TextView text;
        public TextView created_at;
    }
}
