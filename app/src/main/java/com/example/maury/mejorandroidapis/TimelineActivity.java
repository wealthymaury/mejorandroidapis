package com.example.maury.mejorandroidapis;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.maury.mejorandroidapis.db.DBOperations;
import com.example.maury.mejorandroidapis.list.TweetAdapter;
import com.example.maury.mejorandroidapis.models.Tweet;
import com.example.maury.mejorandroidapis.utils.NetworkUtils;
import com.example.maury.mejorandroidapis.utils.TwitterUtils;
import com.example.maury.mejorandroidapis.utils.ConstantsUtils;

import java.util.ArrayList;

public class TimelineActivity extends Activity {

    private ListView lv_timeline;
    private DBOperations dbOperations;
    private TweetAdapter adapter;
    private TimelineReceiver receiver;
    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        lv_timeline = (ListView) findViewById(R.id.lv_timeline);

        dbOperations = new DBOperations(this);
        receiver = new TimelineReceiver();
        filter = new IntentFilter(ConstantsUtils.NEW_TWEETS_INTENT_FILTER);

        // Mandando ejecutar la tarea asincrona
        new GetTimelineTask().execute();
    }

    // Este metodo se lanza despues de onCreate
    // En este metodo debes inicializar suscripciones a servicios o a receivers
    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(receiver, filter);
    }

    // Se ejecuta cuando el usuario le da clic al home
    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    // Asigna el adapter para llenar la lista en pantalla
    private void updateListView(ArrayList<Tweet> tweets){
        adapter = new TweetAdapter(this, R.layout.row_tweet, tweets);
        lv_timeline.setAdapter(adapter);
    }

    // Recarga la lista con los nuevos datos de la DB
    private void updateListViewWithCache(){
        adapter = new TweetAdapter(this, R.layout.row_tweet, dbOperations.getStatusUpdates());
        lv_timeline.setAdapter(adapter);
        adapter.notifyDataSetChanged(); // Avisa para que se repinte el UI
    }

    /*
     *  Esta clase interna sirve para evitar bloquear la interface mientras se traer los twits
     *  es un proceso asincrono que es capaz tambien de refrezcar la pantalla comunicandose
     *  con el UI THREAD
     *
     *  Que significa el <Object, Void, Void>???
     *      El primer parametro es el tipo de dato que se le puede pasar al metodo doInBackground
     *      Es segundo parametro es el tipo de dato que recibe el metodo doInBackground
     *      El tercer parametro es el que devuelve doInBackground y se le pasa a onPostExecute
     *  */
    class GetTimelineTask extends AsyncTask<Object, Void, ArrayList<Tweet>>{
        private ProgressDialog progress_dialog;

        // Este metodo se ejecuta antes de que se ejecute la tarea
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // No puede ser, que facil es pasar un contexto de algo a lo que no tienes acceso :D
            progress_dialog = new ProgressDialog(TimelineActivity.this);
            progress_dialog.setTitle(getResources().getString(R.string.label_tweet_search_loader));
            progress_dialog.show();
        }

        @Override
        protected ArrayList<Tweet> doInBackground(Object... objects) {
            if(NetworkUtils.haveNetworkConnection(TimelineActivity.this)){
                return TwitterUtils.getTimelineForSearchTerm(ConstantsUtils.MEJORANDROID_TERM);
            }else{
                return dbOperations.getStatusUpdates();
            }
        }

        // Este metodo se ejecuta despues de que termino la tarea
        @Override
        protected void onPostExecute(ArrayList<Tweet> timeline) {
            super.onPostExecute(timeline);
            progress_dialog.dismiss();

            if(timeline.isEmpty()){
                Toast.makeText(TimelineActivity.this, getResources().getString(R.string.label_tweets_not_found), Toast.LENGTH_SHORT).show();
            }else{
                updateListView(timeline);
                Toast.makeText(TimelineActivity.this, getResources().getString(R.string.label_tweets_downloaded), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
     * El broadcast receiver debe poder accesar a la interface, por eso se declaro aqu
     *
     */
    class  TimelineReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            updateListViewWithCache();
        }
    }
}
